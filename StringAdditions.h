//
//  StringAdditions.h
//  Nexus
//
//  Created by Peter Schols on Wed Oct 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (StringAdditions)

- (BOOL)hasCaseInsensitivePrefix:(NSString *)prefix;
- (NSString *)stringByReplacingSpaceWithUnderscore;

- (BOOL)stringContains:(NSString *)s;
- (BOOL)stringBeginsWithTwoNumbers;
- (BOOL)stringContainsOneSpace;
- (BOOL)stringContainsHyphen;


- (NSMutableArray *)splitLines;

- (NSString *)stringByRemovingWhitespace;
- (NSString *)stringByRemovingCharactersFromSet:(NSCharacterSet *)set;

@end

@interface NSMutableString(IDStringUtils)

- (void)removeCharactersInSet:(NSCharacterSet *)set;

@end
