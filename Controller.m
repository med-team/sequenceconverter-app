#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "Controller.h"
#include "Interface.h"
#include "BCReader.h"
#include "BCCreator.h"

@implementation Controller: NSObject
{
    Interface *face;
    NSDictionary *matrix;
    NSArray *taxonList;    
}


-(id) init
{
    [super init];
    face = nil;
    return self;
}


-(void) dealloc
{
    if (face) 
        [face release];
    [super dealloc];
}


// Set the corresponding face
-(void) setFace: (Interface *)aFace
{
    face = [aFace retain];
}


// The various buttons 
-(void) clear: (id)sender
{
}


- (void)chooseTextFile;
{
    NSOpenPanel *panel = [NSOpenPanel openPanel];
    //[panel beginForDirectory:nil file:nil types:nil modelessDelegate:nil  didEndSelector:@selector(openPanelDidEnd:returnCode:contextInfo:) contextInfo:nil];
    [panel setCanChooseDirectories:NO];
    [panel setPrompt:@"Choose File"];
    [panel runModalForDirectory:nil file:nil types:nil];
    if ([panel filename]) {
        [self readSequenceFile:[panel filename]];
    }
}


- (void)openPanelDidEnd:(NSOpenPanel *)openPanel returnCode:(int)returnCode contextInfo:(void  *)contextInfo;
{
    if (returnCode == NSOKButton) {
        [[face statusField] setStringValue:[openPanel filename]];
        [openPanel close];
        [self readSequenceFile:[openPanel filename]];
    }
}


// Pass the contents of the sequence file as a string to the BCReader
- (void)readSequenceFile:(NSString *)filePath;
{
    int i, totalColumns;
    NSMutableString *sequenceFile = [NSMutableString stringWithContentsOfFile:filePath];
    NSDictionary *sequenceDict;
    BCReader *reader = [[BCReader alloc]init];
    
    // The BCReader returns an NSDictionary (see BCReader.h)
    sequenceDict = [reader readFile:sequenceFile];
    [reader release];
    
    
    // Set the matrix and taxonList variables
    [self setMatrix:[sequenceDict objectForKey:@"matrix"]];
    [self setTaxonList:[sequenceDict objectForKey:@"items"]];
    NSString *fileType = [sequenceDict objectForKey:@"fileType"];
    
    // Show the number of taxa and characters in the status field
    [[face statusField] setStringValue:[NSString stringWithFormat:@"This %@ file contains %d taxa - %d characters", fileType, [taxonList count], [(NSString *)[matrix objectForKey:[taxonList objectAtIndex:1]]length] ]];
    NSLog(@"%@", taxonList);
}



// Method for saving files

- (void)saveFile;
{
    // Invoke the saveFile method (see BCCreator.h)
    NSString *fileFormat = [[face popupButton]titleOfSelectedItem];
    BCCreator *creator = [[BCCreator alloc]init];
    [creator useLineBreakFromSource:matrix];
    [creator saveFile:matrix withComments:@"Test" extraBlocks:@"BEGIN ASSUMPTIONS;\nOPTIONS  DEFTYPE=unord PolyTcount=MINSTEPS ;\nEND;" fileFormat:fileFormat];
    [creator release];
    [[face statusField] setStringValue:[NSString stringWithFormat:@"The %@ file has been written", fileFormat]];
}



//Accessor methods
- (NSDictionary *) matrix
{
    return matrix;
}

- (void) setMatrix: (NSDictionary *) aMatrix
{
    if (matrix != aMatrix)
    {
        [aMatrix retain];
        [matrix release];
        matrix = aMatrix;
    }
}

- (NSArray *) taxonList
{
    return taxonList;
}

- (void) setTaxonList: (NSArray *) aTaxonList
{
    if (taxonList != aTaxonList)
    {
        [aTaxonList retain];
        [taxonList release];
        taxonList = aTaxonList;
    }
}



@end

