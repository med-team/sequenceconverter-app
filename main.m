#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "Controller.h"
#include "Interface.h"

int main (void)
{ 
    Controller *brain;
    Interface *face;
    NSAutoreleasePool *pool;
    NSApplication *app;
    NSMenu *mainMenu;
    NSMenu *menu;
    NSMenuItem *menuItem;
    
    pool = [NSAutoreleasePool new];
    app = [NSApplication sharedApplication];
    [app setApplicationIconImage: [NSImage imageNamed: @"Calculator.app.tiff"]];
    
    /*
    mainMenu = [[NSMenu new]autorelease];
    // Info
    [mainMenu addItemWithTitle: @"Info..." 
                        action: @selector (orderFrontStandardInfoPanel:) 
                 keyEquivalent: @""];
    // Edit SubMenu
    menuItem = [mainMenu addItemWithTitle: @"Edit" 	
                                   action: NULL 
                            keyEquivalent: @""];
    menu = [[NSMenu new]autorelease];
    [mainMenu setSubmenu: menu forItem: menuItem];
      [menu addItemWithTitle: @"Copy" 
                    action: @selector (copy:) 
             keyEquivalent: @"c"];
       [menu addItemWithTitle: @"SelectAll" 
                    action: @selector (selectAll:) 
             keyEquivalent: @"a"];
    
    [mainMenu addItemWithTitle: @"Hide" 
                        action: @selector (hide:) 
                 keyEquivalent: @"h"];  
    [mainMenu addItemWithTitle: @"Quit" 
                        action: @selector (terminate:)
                 keyEquivalent: @"q"];	
    
    [app setMainMenu: mainMenu];
     */
    
    brain = [Controller new];
    face = [Interface new]; 
    [brain setFace: face];
    [face setBrain: brain];
    [app setDelegate: face];
    
    [app run];
    return 0;
}

