//
//  StringAdditions.m
//  Nexus
//
//  Created by Peter Schols on Wed Oct 22 2003.
//  Copyright (c) 2003 __MyCompanyName__. All rights reserved.
//
//
//  Permission to use, copy, modify and distribute this software and its documentation
//  is hereby granted, provided that both the copyright notice and this permission
//  notice appear in all copies of the software, derivative works or modified versions,
//  and any portions thereof, and that both notices appear in supporting documentation,
//  and that credit is given to Erik Doernenburg in all documents and publicity
//  pertaining to direct or indirect use of this code or its derivatives.


#import "StringAdditions.h"


@implementation NSString (StringAdditions)

-(BOOL)hasCaseInsensitivePrefix:(NSString *)prefix
{
    if ([self rangeOfString:prefix options:NSCaseInsensitiveSearch range:NSMakeRange(0,[prefix length])].location != NSNotFound) {
        return YES;
    }
    else { return NO;}
}

-(NSString *)stringByReplacingSpaceWithUnderscore
{
    NSMutableString *ms = [NSMutableString stringWithString:self];
    [ms replaceOccurrencesOfString:@" " withString:@"_" options:NULL range:NSMakeRange(0, [self length])];
    return ms;
}

-(BOOL)stringContains:(NSString *)s
{
    NSRange	aRange;
    aRange = [self rangeOfString:s];
    return (aRange.location != NSNotFound);
}


- (BOOL)stringContainsOneSpace
{
    NSArray *array = [self componentsSeparatedByString:@" "];
    if ([array count] == 2) {
        return YES;
    }
    else { return NO; }
}


- (BOOL)stringContainsHyphen
{
    NSRange	aRange;
    aRange = [self rangeOfString:@"'"];
    return (aRange.location != NSNotFound);
}


-(BOOL)stringBeginsWithTwoNumbers
{
	NSScanner   *scanner = [NSScanner scannerWithString: self];
	
	if ([scanner scanInt:nil])
	{
		if ([scanner scanInt:nil])
			return YES;
		else
			return NO;
	}
	
	else
		return NO;
}


- (NSMutableArray *)splitLines
{
    NSMutableArray	*arrayOfLines = [[NSMutableArray alloc] init];

    unsigned	stringLength = [self length];
    NSRange	lineRange = NSMakeRange(0, 0);
    NSRange	searchRange = NSMakeRange(0, 0);
    
    while ( searchRange.location < stringLength )
    {
		[self getLineStart:nil end: &searchRange.location contentsEnd: nil forRange: searchRange];
		lineRange.length = searchRange.location - lineRange.location;

		[arrayOfLines addObject:[self substringWithRange: lineRange]];
		lineRange.location = searchRange.location;
    }

    return [arrayOfLines autorelease];
}

- (NSString *)stringByRemovingWhitespace
{
    return [self stringByRemovingCharactersFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *)stringByRemovingCharactersFromSet:(NSCharacterSet *)set
{
    NSMutableString	*temp;

    if([self rangeOfCharacterFromSet:set options:NSLiteralSearch].length == 0)
        return self;
    
    temp = [[self mutableCopyWithZone:[self zone]] autorelease];
    [temp removeCharactersInSet:set];

    return temp;
}


@end

@implementation NSMutableString(IDStringUtils)

- (void)removeCharactersInSet:(NSCharacterSet *)set
{
    NSRange		matchRange, searchRange, replaceRange;
    unsigned int	length;

    length = [self length];
    matchRange = [self rangeOfCharacterFromSet:set options:NSLiteralSearch range:NSMakeRange(0, length)];
    
    while(matchRange.length > 0)
    {
        replaceRange = matchRange;
        searchRange.location = NSMaxRange(replaceRange);
        searchRange.length = length - searchRange.location;
        
        for(;;)
        {
            matchRange = [self rangeOfCharacterFromSet:set options:NSLiteralSearch range:searchRange];
            if((matchRange.length == 0) || (matchRange.location != searchRange.location))
                break;
            replaceRange.length += matchRange.length;
            searchRange.length -= matchRange.length;
            searchRange.location += matchRange.length;
        }
        
        [self deleteCharactersInRange:replaceRange];
        matchRange.location -= replaceRange.length;
        length -= replaceRange.length;
    }
}

@end
