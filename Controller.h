
@class Interface;

@interface Controller: NSObject
{
    Interface *face;
    NSDictionary *matrix;
    NSArray *taxonList;
}


// Set the corresponding face
-(void) setFace: (Interface *)aFace;


    // Action methods
- (void)chooseTextFile;
- (void)readSequenceFile:(NSString *)filePath;
- (void)saveFile;


    //Accessor methods
- (NSDictionary *) matrix;
- (void) setMatrix: (NSDictionary *) aMatrix;
- (NSArray *) taxonList;
- (void) setTaxonList: (NSArray *) aTaxonList;

@end

