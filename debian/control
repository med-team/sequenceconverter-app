Source: sequenceconverter.app
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Scott Christley <schristley@mac.com>,
           Andreas Tille <tille@debian.org>,
           Charles Plessy <plessy@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10),
               libgnustep-gui-dev,
               imagemagick
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/med-team/sequenceconverter-app
Vcs-Git: https://salsa.debian.org/med-team/sequenceconverter-app.git
Homepage: http://bioinformatics.org/biococoa/

Package: sequenceconverter.app
Architecture: any
Depends: ${shlibs:Depends},
         ${gnustep:Depends}
Description: biological sequence file format conversion applet for GNUstep
 Demo application to demonstrate the possibilities of the BioCocoa framework.
 .
 SequenceConverter is a GNUstep applet to convert between sequence file formats.
 The BioCocoa framework provides developers with the opportunity to add
 support for reading and writing BEAST, Clustal, EMBL, Fasta, GCG-MSF, GDE,
 Hennig86, NCBI, NEXUS, NONA, PDB, Phylip, PIR, Plain/Raw, Swiss-Prot and
 TNT files by writing only three lines of code.
