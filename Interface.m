
#include <Foundation/Foundation.h>
#include <AppKit/AppKit.h>
#include "Interface.h"



@implementation Interface: NSWindow
{
    NSButton *openButton;
    NSButton *saveButton;
    NSPopUpButton *popupButton;
    NSTextField *statusField;
}

-(id)init
{
    int i;
    
    // Display 
    statusField = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 15, 360, 24)];
    [statusField setEditable: NO];
    //[statusField setScrollable: YES];
    [statusField setAlignment:NSCenterTextAlignment];
    [statusField setBezeled: NO];
    [statusField setDrawsBackground: NO];
    
    NSTextField *commentField1 = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 300, 360, 15)];
    [commentField1 setEditable: NO];
    [commentField1 setAlignment:NSNaturalTextAlignment];
    [commentField1 setBezeled: NO];
    [commentField1 setDrawsBackground: NO];
    [commentField1 setStringValue:@"This is a demo application to demonstrate the possibilities"];
    
          
    NSTextField *commentField2 = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 285, 360, 15)];
    [commentField2 setEditable: NO];
    [commentField2 setAlignment:NSLeftTextAlignment];
    [commentField2 setBezeled: NO];
    [commentField2 setDrawsBackground: NO];
    [commentField2 setStringValue:@"of the BioCocoa framework. While useful in itself,"];
    
    NSTextField *commentField3 = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 270, 360, 15)];
    [commentField3 setEditable: NO];
    [commentField3 setAlignment:NSLeftTextAlignment];
    [commentField3 setBezeled: NO];
    [commentField3 setDrawsBackground: NO];
    [commentField3 setStringValue:@"this application shows how easy it is to add support"];
    
    NSTextField *commentField4 = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 255, 360, 15)];
    [commentField4 setEditable: NO];
    [commentField4 setAlignment:NSLeftTextAlignment];
    [commentField4 setBezeled: NO];
    [commentField4 setDrawsBackground: NO];
    [commentField4 setStringValue:@"for reading and writing 10+ file formats."];

    
    NSTextField *formatField = [[NSTextField alloc] initWithFrame: NSMakeRect (20, 127, 120, 24)];
    [formatField setEditable: NO];
    [formatField setAlignment:NSLeftTextAlignment];
    [formatField setBezeled: NO];
    [formatField setDrawsBackground: NO];
    [formatField setStringValue:@"Select file format:"];
    
    
    // Buttons
    openButton = [[NSButton alloc] initWithFrame: NSMakeRect (20, 200, 360, 24)];
    [openButton setButtonType: NSToggleButton];
    [openButton setTitle: @"Open file to convert"];
    [openButton setTag: 0];
    [openButton setState: NO];
    [openButton setAction: @selector(chooseTextFile)];
    [openButton setKeyEquivalent: @"O"];
    
    saveButton = [[NSButton alloc] initWithFrame: NSMakeRect (20, 100, 360, 24)];
    [saveButton setButtonType: NSToggleButton];
    [saveButton setTitle: @"Export file in the selected format"];
    [saveButton setTag: 1];
    [saveButton setState: NO];
    [saveButton setAction: @selector(saveFile)];
    [saveButton setKeyEquivalent: @"E"];
    
    popupButton = [[NSPopUpButton alloc] initWithFrame: NSMakeRect (150, 130, 230, 24) pullsDown:NO];
    [popupButton addItemWithTitle:@"Beast XML"];
    [popupButton addItemWithTitle:@"Clustal"];
    [popupButton addItemWithTitle:@"Fasta"];
    [popupButton addItemWithTitle:@"GCG-MSF"];
    [popupButton addItemWithTitle:@"GDE"];
    [popupButton addItemWithTitle:@"Hennig86"];
    [popupButton addItemWithTitle:@"Nexus"];
    [popupButton addItemWithTitle:@"NONA"];
    [popupButton addItemWithTitle:@"Phylip"];
    [popupButton addItemWithTitle:@"PIR"];
    [popupButton addItemWithTitle:@"Plain/Raw"];
    [popupButton addItemWithTitle:@"TNT"];

    
    // Window
    [self initWithContentRect: NSMakeRect (100, 100, 400, 330)
                    styleMask: (NSTitledWindowMask | NSMiniaturizableWindowMask | NSClosableWindowMask)
                      backing: NSBackingStoreBuffered
                        defer: NO];
    
    [self setBackgroundColor:[NSColor colorWithCalibratedRed:0.9254 green:0.91372 blue:0.8470 alpha:1.0]];
    
    [self setInitialFirstResponder: openButton];
    
    [[self contentView] addSubview: openButton];
    [[self contentView] addSubview: saveButton];
    [[self contentView] addSubview: popupButton];
    [openButton release];
    [saveButton release];
    [popupButton release];

    
    [[self contentView] addSubview: statusField];
    [statusField release];
    [[self contentView] addSubview: commentField1];
    [commentField1 release];
    [[self contentView] addSubview: commentField2];
    [commentField2 release];
    [[self contentView] addSubview: commentField3];
    [commentField3 release];
    [[self contentView] addSubview: commentField4];
    [commentField4 release];

    [[self contentView] addSubview: formatField];
    [formatField release];

    [self setTitle: @"BioCocoa"];
    [self center];
    
    return self;
}


- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
    return YES;   
}



-(void) dealloc
{
    [super dealloc];
}

-(void) setBrain: (Controller *)aBrain
{
    [openButton setTarget: aBrain];
    [saveButton setTarget: aBrain];
    [popupButton setTarget: aBrain];

}


- (void)applicationDidFinishLaunching: (NSNotification *)aNotification
{
    [self orderFront: self];
}


- (NSTextField *) statusField {
    return statusField; 
}

- (void) setStatusField: (NSTextField *) aStatusField {
    [aStatusField retain];
    [statusField release];
    statusField = aStatusField;
}



- (NSPopUpButton *) popupButton
{
    return popupButton; 
}

- (void) setPopupButton: (NSPopUpButton *) aPopupButton
{
    if (popupButton != aPopupButton) {
        [aPopupButton retain];
        [popupButton release];
        popupButton = aPopupButton;
    }
}

@end


