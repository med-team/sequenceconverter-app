

@class Controller;

@interface Interface: NSWindow
{  
    NSButton *openButton;
    NSButton *saveButton;
    NSPopUpButton *popupButton;
    NSTextField *statusField;
}

// Set the corresponding brain
-(void) setBrain: (Controller *)aBrain;
- (void)applicationDidFinishLaunching: (NSNotification *)aNotification;

//Aaccessors
- (NSTextField *) statusField;
- (void) setStatusField: (NSTextField *) aStatusField;

- (NSPopUpButton *) popupButton;
- (void) setPopupButton: (NSPopUpButton *) aPopupButton;


@end

