Compiling and using BioCocoa on Linux

In order to compile and use BioCocoa on Linux, the GNUstep libraries are required. See the steps below to download and compile GNUstep. If GNUstep is already installed on your machine, you can proceed to step 4.

1. Downloading GNUstep
Point your browser to: http://www.gnustep.org/resources/sources.html and download the appropriate archive.

2. Building and installing GNUstep
Detailed instructions on how to build GNUstep on unix based systems is available at: http://gnustep.made-it.com/BuildGuide/

3. Running some GNUstep daemons
GNUstep requires that two daemons are launched at boot time. See Chapter 6 about GNUstep services at: http://gnustep.made-it.com/BuildGuide/#GNUSTEP.SERVICES 

4. Compiling BioCocoa
You are now ready to compile and run BioCocoa. cd to the BioCocoa directory and enter make. The BioCocoa libraries and the demo application will then be compiled.


More information about GNUstep: http://www.gnustep.org/
More information about BioCocoa: http://bioinformatics.org/biococoa/
Contact: peter.schols@bio.kuleuven.ac.be